import CompilerComponents.Tokenizer;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * Tests the {@link Tokenizer} with random series of strings meaning to represent edge cases and regular examples
 */
public class TokenizerTests
{
    //Test String
    private static final String[] STRING_TESTS =
            {
                    "123 + 56 * num1",
                    "(1+23.0)*.9",
                    "aa1 = (14 - 3) * 2 / a23",
                    "1 + (2 + 1) * (78+3*15)",
                    "aa1 = (14 -3) // 2",
                    "for i in range(3):",
                    "2 + 3.2.5 - 56**a.num1",
                    "1.25e+10",
                    "1.25e+ 10",
                    "1.25e++10"
            };
    //String array Answers
    private static final String[][] ANSWERS =
            {
                    {"123","+","56","*","num1"},
                    {"(","1","+","23.0",")","*",".9"},
                    {"aa1","=","(","14","-","3",")","*", "2", "/", "a23"},
                    {"1","+","(","2","+","1",")","*", "(","78","+","3","*","15",")"},
                    {"aa1","=","(","14","-","3",")","/", "/", "2"},
                    {"for", "i", "in", "range","(","3",")",":"},
                    {"2","+","3.2.5","-","56","*", "*","a.num1"},
                    {"1.25e+10"},
                    {"1.25e+, 10"},
                    {"1.25e+, +, 10"}
            };

    @Test
    public void eqsPlusEquation()
    {
        for (int i = 0; i < STRING_TESTS.length; i++)
            Assert.assertEquals(strArray(ANSWERS[i]),strArray(Tokenizer.tokenize(STRING_TESTS[i])));
    }

    private String strArray(String[] s)
    {
        return Arrays.toString(s);
    }
}
