import CompilerComponents.HelperComponents.Identifier;
import Enums.ParserEnum;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test the {@link Identifier} by via a series of string and determining their correct {@link ParserEnum} value.
 */
public class IdentifierTests
{
    /* VALID TEST */
    //basic numbers
    private static final String[] VAL_BASIC_NUMBERS =
            {
                    "42",
                    "10.1",
                    ".9",
                    "0.10",
                    "12."
            };

    //Scientific numbers
    private static final String[] VAL_SCIENTIFIC_NUMBERS =
            {
                    "1.25e4",
                    "1.25e+9",
                    "1.25e10",
                    "1.25e-7",
                    "1.25e-12"
            };

    //Variables
    private static String[] VAL_VARAIABLES =
            {
                    "variable",
                    "variable123",
                    "asd123as123",
            };

    //Functions
    private static String[] VAL_OBJECT_FUNCTION_CALLS =
            {
                    "obj.function",
                    "obj123.function",
                    "obj123.function123",
                    "obj.function123"
            };

    /* INVALID TEST*/
    //basic Numbers
    private static final String[] INVAL_BASIC_NUMBERS =
    {
            "2.0.5",
            "a.20.05"
    };

    //Scientific Numbers
    private static final String[] INVAL_SCIENTIFIC_NUMBERS =
            {
                    "1.25e-+10",
                    "1.25e--10",
                    "1.25e++10",
                    "1.25e"
            };

    //Variables & Functions
    private static String[] INVAL_VARIABLES_FUNCTIONS =
            {
                    "123asd",
                    "123asd123asd",
                    "123obj.function",
                    "obj.123function",
                    "123obj.123obj"
            };

    /*TESTING FUNCTIONS*/
    /**
     * Tests the strings of {@code VAL_BASIC_NUMBERS} to ensure that they all match to {@link Enums.ParserEnum}.variable.
     */
    @Test
    public void testValBasicNums()
    {
        for (String s: VAL_BASIC_NUMBERS)
        {
            Assert.assertEquals(ParserEnum.number, Identifier.whatIs(s));
        }
    }

    /**
     * Tests the strings of {@code VAL_SCIENTIFIC_NUMBERS} to ensure that they all match to {@link Enums.ParserEnum}.variable.
     */
    @Test
    public void testValScientificNumbers()
    {
        for (String s: VAL_SCIENTIFIC_NUMBERS)
        {
            Assert.assertEquals(ParserEnum.number, Identifier.whatIs(s));
        }
    }

    /**
     * Tests the strings of {@code VAL_VARAIABLES} to ensure that they all match to {@link Enums.ParserEnum}.variable.
     */
    @Test
    public void testValVariables()
    {
        for (String s: VAL_VARAIABLES)
        {
            Assert.assertEquals(ParserEnum.variable, Identifier.whatIs(s));
        }
    }

    /**
     * Tests the strings of {@code VAL_OBJECT_FUNCTION_CALLS} to ensure that they all match to {@link Enums.ParserEnum}.variable.
     */
    @Test
    public void testValObjFunctionCalls()
    {
        for (String s: VAL_OBJECT_FUNCTION_CALLS)
        {
            Assert.assertEquals(ParserEnum.objectFunctionCall, Identifier.whatIs(s));
        }
    }

    /**
     * Tests that all operators found in {@code valOperators} return there proper enum assignment.
     */
    @Test
    public void testOperators()
    {
        Assert.assertEquals(ParserEnum.leftParentheses, Identifier.whatIs("("));
        Assert.assertEquals(ParserEnum.rightParenthesis, Identifier.whatIs(")"));
        Assert.assertEquals(ParserEnum.unaryOperator, Identifier.whatIs("+"));
        Assert.assertEquals(ParserEnum.unaryOperator, Identifier.whatIs("-"));
        Assert.assertEquals(ParserEnum.multiplier, Identifier.whatIs("*"));
        Assert.assertEquals(ParserEnum.divisor, Identifier.whatIs("/"));
        Assert.assertEquals(ParserEnum.modulus, Identifier.whatIs("%"));
        Assert.assertEquals(ParserEnum.equals, Identifier.whatIs("="));
        Assert.assertEquals(ParserEnum.leftCarrot, Identifier.whatIs("<"));
        Assert.assertEquals(ParserEnum.rightCarrot, Identifier.whatIs(">"));
    }

    /**
     * Tests the strings of {@code INVAL_BASIC_NUMBERS} to ensure that they all match to {@link ParserEnum}.inValid.
     */
    @Test
    public void testInvalBasicNumbers()
    {
        for (String s: INVAL_BASIC_NUMBERS)
        {
            Assert.assertEquals(ParserEnum.inValid, Identifier.whatIs(s));
        }
    }

    /**
     * Tests the strings of {@code INVAL_SCIENTIFIC_NUMBERS} to ensure that they all match to {@link ParserEnum}.inValid.
     */
    @Test
    public void testInvalScientificNumbers()
    {
        for (String s: INVAL_SCIENTIFIC_NUMBERS)
        {
            Assert.assertEquals(ParserEnum.inValid, Identifier.whatIs(s));
        }
    }

    /**
     * Tests the strings of {@code INVAL_VARIABLES_FUNCTIONS} to ensure that they all match to {@link ParserEnum}.inValid.
     */
    @Test
    public void testInvalVariablesAndFunctions()
    {
        for (String s: INVAL_VARIABLES_FUNCTIONS)
        {
            Assert.assertEquals(ParserEnum.inValid, Identifier.whatIs(s));
        }
    }
}
