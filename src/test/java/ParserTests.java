import CompilerComponents.Parser;
import CompilerComponents.Tokenizer;
import Enums.ParserEnum;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test class to evaluate whether or not a series of String are parsible by using the {@link CompilerComponents.Parser}
 */
public class ParserTests
{
    /* VALID TEST */
    //basic numbers
    private static final String[] VAL_BASIC_NUMBERS =
            {
                    "42",
                    "10.1",
                    ".9",
                    "0.10",
                    "12."
            };

    //Scientific numbers
    private static final String[] VAL_SCIENTIFIC_NUMBERS =
            {
                    "1.25e4",
                    "1.25e+9",
                    "1.25e10",
                    "1.25e-7",
                    "1.25e-12"
            };

    //Variables
    private static String[] VAL_VARIABLES =
            {
                    "variable",
                    "variable123",
                    "asd123as123",
            };

    /* INVALID TEST*/
    //basic Numbers
    private static final String[] INVAL_BASIC_NUMBERS =
            {
                    "2.0.5",
                    "a.20.05"
            };

    //Scientific Numbers
    private static final String[] INVAL_SCIENTIFIC_NUMBERS =
            {
                    "1.25e-+10",
                    "1.25e--10",
                    "1.25e++10",
                    "1.25e"
            };

    //Variables & Functions
    private static String[] INVAL_VARIABLES =
            {
                    "123asd",
                    "123asd123asd",
            };

    /*COMPLICATED STRING PATTERN TESTS TRUE*/
    //Simple unary operations
    private static String[] VAL_SIMPLE_UNARY_OPERATORS =
            {
                    "6 + 6",
                    "6 - 6",
                    "6 +- 6",
                    "6 -+ 6",
                    "6 +-+ 6",
                    "6 -+- 6",
                    "(6) + 6",
                    "6 + (6)",
                    "(6) + (6)",
                    "(5 + 5)",
                    "6 + 6 + 5",
                    "6 - 6 + 5",
                    "6 +- 6 + 5",
                    "6 -+ 6 + 5",
                    "6 +-+ 6 + 5",
                    "6 -+- 6 + 5",
                    "(6) + 6 + 5",
                    "6 + (6) + 5",
                    "6 + 6 + (5)",
                    "6 + (6 + 5)",
                    "(6) + (6) + 5",
                    "(5 + 5) + 5"
            };
    //Signed number patterns
    private static String[] VAL_SIGNED_PATTERNS =
            {
                    "+5 -+ 10",
                    "+5 ** 10",
                    "+5 // 10",
                    "+5 % 10",
                    "-5 -+ 10",
                    "-5 ** 10",
                    "-5 // 10",
                    "-5 % 10",

                    "(+5) -+ 10",
                    "(+5) ** 10",
                    "(+5) // 10",
                    "(+5) % 10",
                    "(-5) -+ 10",
                    "(-5) ** 10",
                    "(-5) // 10",
                    "(-5) % 10",

                    "- (variable)",
                    "+ (variable)",
            };

    //Functions and object calls
    private static String[] VAL_OBJECT_FUNCTION_CALLS =
            {
                    "obj.function()",
                    "obj123.function()",
                    "obj123.function123()",
                    "obj.function123()",
                    "obj.function((7 * 8))",
                    "obj123.function((7 * 8))",
                    "obj123.function123((7 * 8))",
                    "obj.function123((7 * 8))",
                    "function()",
                    "function123()",
                    "function((7 * 8))",
                    "function123((7 * 8))",
                    "obj.function() + 5",
                    "obj123.function() + 5",
                    "obj123.function123() + 5",
                    "obj.function123() + 5",
            };

    //simple multiplier operations
    private static String[] VAL_SIMPLE_MULTIPLIER_OPERATIONS =
            {
                    "6 * 6",
                    "6 ** 6",
                    "(6) * 6",
                    "6 * (6)",
                    "(6) * (6)",
                    "(5 * 5)",
                    "6 * 6 * 5",
                    "6 ** 6 * 5",
                    "6 ** 6 ** 5",
                    "6 * 6 ** 5",
                    "(6) * 6 * 5",
                    "6 * (6) * 5",
                    "(6) * (6) * 5",
                    "6 * 6 * (5)",
                    "6 * (6 * 5)",
                    "(5 * 5) * 5",
            };

    //simple divisor operations
    private static String[] VAL_SIMPLE_DIVISOR_OPERATIONS =
            {
                    "6 / 6",
                    "6 // 6",
                    "(6) / 6",
                    "6 / (6)",
                    "(6) / (6)",
                    "(5 / 5)",
                    "6 / 6 / 5",
                    "6 // 6 / 5",
                    "6 // 6 // 5",
                    "6 / 6 // 5",
                    "(6) / 6 // 5",
                    "6 / (6) / 5",
                    "6 / 6 / (5)",
                    "6 / (6 / 5)",
                    "(6) / (6) / 5",
                    "(6 / 6) / 5",
            };

    //simple modulo operations
    private static String[] VAL_SIMPLE_MODULO_OPERATIONS =
            {
                    "6 % 6",
                    "(6) % 6",
                    "6 % (6)",
                    "(6) % (6)",
                    "(5 % 5)",
                    "6 % 6 % 5",
                    "(6) % 6 % 5",
                    "6 % (6) % 5",
                    "6 % 6 % (5)",
                    "6 % (6 % 5)",
                    "(6) % (6) % 5",
                    "(6 % 6) % 5",
            };

    //Confirms that valid parenthesis functions work.
    private static String[] VAL_PARENTHESIS =
            {
                    "(5) + 5",
                    "5 + (5)",
                    "(5) + (5)",
                    "(5 + 5)",
                    "((5) + 5)",
                    "(5 + (5))",
                    "((5) + (5))",
                    "((5 + 5))",
                    "5 + (5) + 5",
                    "(5 + (5) + 5)",
                    "((5) + (5) + 5)",
                    "(5 + (5) + (5))",
                    "((5) + (5) + (5))",
                    "(((5)))",
            };

    //Combination of all the above patterns,
    private static String[] VAL_COMBO_PATTERN =
            {
                    "13 + val + bob - tim * billy / 14 % thick123 + 12",
                    "13 + val -+ bob - tim * billy / 14 % thick123 + 12",
                    "13 + val -+ bob +- tim * billy / 14 % thick123 + 12",
                    "13 + val -+ bob +- tim ** billy / 14 % thick123 + 12",
                    "13 + val -+ bob +- tim ** billy // 14 % thick123 + 12",
                    "13 + val - bob +- tim ** billy // 14 % thick123 + 12",
                    "13 + val - bob + tim ** billy // 14 % thick123 + 12",
                    "13 + val - bob + tim * billy // 14 % thick123 + 12",
                    "13 + val - bob + tim * billy / 14 % thick123 + 12",
            };

    //All the proper ways to call a while loop.
    private static final String[] VAL_WHILE_LOOP_CALLS =
            {
                    "while(true):",
                    "while(variable):",
                    "while(17):",
                    "while(obj.function()):",
                    "while(obj.function(true)):",
                    "while(obj.function(17)):",
                    "while(function()):",
                    "while(function(true)):",
                    "while(function(17)):",
            };

    //All the proper ways to call a forward loop.
    private static final String[] VAL_FORWARD_LOOP_CALLS =
            {
                    "for i in range(true):",
                    "for i in range(variable):",
                    "for i in range(17):",
                    "for i in range((17 + 2)):",
                    "for i in range(17 * 5):",
                    "for i in range((+12)/ 3):",
                    "for i in range(obj.function()):",
                    "for i in range(obj.function(true)):",
                    "for i in range(obj.function(17)):",
                    "for i in range(function()):",
                    "for i in range(function(true)):",
                    "for i in range(function(17)):",
            };
    private static final String[] VAL_ASSIGNMENT_CALLS =
            {
                    "a = 10",
                    "asd123 = variable",
                    "asd123 = (5 + 4)",
                    "asd123 = (5 - 4)",
                    "asd123 = (5 / 4)",
                    "asd123 = (5 * 4)",
                    "asd123 = (5 // 4)",
                    "asd123 = (5 ** 4)",
                    "asd123 = (5 % 4)",
                    "asd123 = (5 * 4) + 5",

            };

    /*COMPLICATED STRING PATTERN TESTS FALSE*/
    //Basic failed unary operator pattern
    private static final String[] INVAL_UNARY_OPERATOR =
            {
                    "5 + 5 +",
                    "5 + 5 -",
                    "5 - 5 +",
                    "5 - 5 -",
                    "5 +- 5 +",
                    "5 -+- 5 +",
                    "5 - 5 +-",
                    "5 - 5 +-+",
                    "5 +* 5",
                    "5 *+ 5",
                    "5 +*+ 5"
            };

    //Basic failed signed pattern
    private static final String[] INVAL_SIGNED_NUMBER =
            {
                    "*17",
                    "/17",
                    "%29",
            };
    //Failed object called operator
    private static final String[] INVAL_OBJ_FUNCTION_CALLS =
            {
                    "function(71",
                    "function(",
                    "obj.function(71",
                    "obj.function(",
                    "123function()",
                    "123function()",
                    "123obj.123function()",
                    "123obj.function()",
                    "obj.123function()",
            };

    //Failed advanced operators, mostly over calling.
    private static final String[] INVAL_ADVANCED_OPERATORS =
            {
                    "5 *** 5",
                    "5 /// 5",
                    "5 %% 5",
                    "5 *** (5)",
                    "5 /// (5)",
                    "5 %% (5)",
                    "(5) *** 5",
                    "(5) /// 5",
                    "(5) %% 5",
                    "(5) *** (5)",
                    "(5) /// (5)",
                    "(5) %% (5)",
            };

    //Failed parenthesis combinations.
    private static final String[] INVAL_PARENTHESIS =
            {
                    "(5  ",
                    "10 )",
                    "+)",
                    "-)",
                    "/)",
                    "*)",
                    "%)",
                    "(10 ",
                    "(+",
                    "(-",
                    "(/",
                    "(*",
                    "(%",
                    "5 + 5)",
                    "5 - 5)",
                    "5 / 5)",
                    "5 * 5)",
                    "5 % 5)",
                    "(5 + 5",
                    "(5 - 5",
                    "(5 / 5",
                    "(5 * 5",
                    "(5 % 5",
                    "(((5)",
                    "((5)))",
                    "(5))))",
                    "( 5 + ())",
                    "(() * 5(",
                    "5 + ( 4",
                    "5 + ) 5",
                    "5 ( + 10",
                    "5) + 10",
            };
    //Improper forward loop calling
    private static final String[] INVAL_FORWARD_LOOPS =
            {
                    "for i in range():",
                    "(for i in range(10)):",
                    "for 6 in range(12):",
                    "17 + for i in range(10):",
                    "17 - for i in range(10):",
                    "17 * for i in range(10):",
                    "17 / for i in range(10):",
                    "17 % for i in range(10):",
                    "for i in range(10): + 77",
                    "for i in range(10): - 77",
                    "for i in range(10): * 77",
                    "for i in range(10): / 77",
                    "for i in range(10): % 77",
            };

    //Improper forward while calling
    private static final String[] INVAL_WHILE_LOOPS =
            {
                    "while()",
                    "(while(true))",
                    "17 + while(true)",
                    "17 - while(true)",
                    "17 * while(true)",
                    "17 / while(true)",
                    "17 % while(true)",
                    "while(true) + 77",
                    "while(true) - 77",
                    "while(true) * 77",
                    "while(true) / 77",
                    "while(true) % 77",
            };

    private static final String[] INVAL_ASSIGNMENT_CALLS =
            {
                    "a == 10",
                    "asd123 = 123variable",
                    "asd123 = 5 + 4)",
                    "asd123 = (5 - 4",
                    "asd123 = ()",
                    "asd123 = (5 *** 4)",
                    "asd123 = (5 /// 4)",
                    "asd123 = (5 */ 4)",
                    "asd123 = (5 /* 4)",
                    "asd123 = (5 %% 4)",
                    "asd123 = (5 * 4) *** 5",

            };

    /*TESTING FUNCTIONS*/
    /**
     * Tests the strings of {@code VAL_BASIC_NUMBERS} to ensure that they all match to {@link Enums.ParserEnum}.variable.
     */
    @Test
    public void testValBasicNums()
    {
        for (String s: VAL_BASIC_NUMBERS)
        {
            Assert.assertTrue(Parser.parse(s));
        }
    }

    /**
     * Tests the strings of {@code VAL_SCIENTIFIC_NUMBERS} to ensure that they all match to {@link Enums.ParserEnum}.variable.
     */
    @Test
    public void testValScientificNumbers()
    {
        for (String s: VAL_SCIENTIFIC_NUMBERS)
        {
            Assert.assertTrue(Parser.parse(s));
        }
    }

    /**
     * Tests the strings of {@code VAL_VARIABLES} to ensure that they all match to {@link Enums.ParserEnum}.variable.
     */
    @Test
    public void testValVariables()
    {
        for (String s: VAL_VARIABLES)
        {
            Assert.assertTrue(Parser.parse(s));
        }
    }

    /**
     * Tests the strings of {@code INVAL_BASIC_NUMBERS} to ensure that they all match to {@link ParserEnum}.inValid.
     */
    @Test
    public void testInvalBasicNumbers()
    {
        for (String s: INVAL_BASIC_NUMBERS)
        {
            Assert.assertFalse(Parser.parse(s));
        }
    }

    /**
     * Tests the strings of {@code INVAL_SCIENTIFIC_NUMBERS} to ensure that they all match to {@link ParserEnum}.inValid.
     */
    @Test
    public void testInvalScientificNumbers()
    {
        for (String s: INVAL_SCIENTIFIC_NUMBERS)
        {
            Assert.assertFalse(Parser.parse(s));
        }
    }

    /**
     * Tests the strings of {@code INVAL_VARIABLES} to ensure that they all match to {@link ParserEnum}.inValid.
     */
    @Test
    public void testInvalVariables()
    {
        for (String s: INVAL_VARIABLES)
        {
            Assert.assertFalse(Parser.parse(s));
        }
    }

    /*HERE IS WHERE MORE COMPLICATED TEST BEGIN.*/
    /**
     * Tests the strings of {@code VAL_SIMPLE_UNARY_OPERATORS} to ensure that they all return true.
     */
    @Test
    public void testValSimpleUnaryPatterns()
    {
        for (String s: VAL_SIMPLE_UNARY_OPERATORS)
        {
            Assert.assertTrue(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code VAL_SIMPLE_UNARY_OPERATORS} to ensure that they all return true.
     */
    @Test
    public void testValSignedPattern()
    {
        for (String s: VAL_SIGNED_PATTERNS)
        {
            Assert.assertTrue(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code VAL_OBJECT_FUNCTION_CALLS} to ensure that they all return true.
     */
    @Test
    public void testValObjFunctionCalls()
    {
        for (String s: VAL_OBJECT_FUNCTION_CALLS)
        {
            Assert.assertTrue(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code VAL_SIMPLE_MULTIPLIER_OPERATIONS} to ensure that they all return true.
     */
    @Test
    public void testValSimpleMultiplierPattern()
    {
        for (String s: VAL_SIMPLE_MULTIPLIER_OPERATIONS)
        {
            Assert.assertTrue(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code VAL_SIMPLE_DIVISOR_OPERATIONS} to ensure that they all return true.
     */
    @Test
    public void testValSimpleDivisorPattern()
    {
        for (String s: VAL_SIMPLE_DIVISOR_OPERATIONS)
        {
            Assert.assertTrue(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code VAL_SIMPLE_MODULO_OPERATIONS} to ensure that they all return true.
     */
    @Test
    public void testValSimpleModuloPattern()
    {
        for (String s: VAL_SIMPLE_MODULO_OPERATIONS)
        {
            Assert.assertTrue(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code VAL_PARENTHESIS} to ensure that they all return true.
     */
    @Test
    public void testValValidParenthesis()
    {
        for (String s: VAL_PARENTHESIS)
        {
            Assert.assertTrue(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code VAL_COMBO_PATTERN} to ensure that they all return true.
     */
    @Test
    public void testValComboPatterns()
    {
        for (String s: VAL_COMBO_PATTERN)
        {
            Assert.assertTrue(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code VAL_WHILE_LOOP_CALLS} to ensure that they all return true.
     */
    @Test
    public void testValWhileLoopCalls()
    {
        for (String s: VAL_WHILE_LOOP_CALLS)
        {
            Assert.assertTrue(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code VAL_FORWARD_LOOP_CALLS} to ensure that they all return true.
     */
    @Test
    public void testValForwardLoopCalls()
    {
        for (String s: VAL_FORWARD_LOOP_CALLS)
        {
            Assert.assertTrue(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code VAL_ASSIGNMENT_CALLS} to ensure that they all return true.
     */
    @Test
    public void testValAssignmentCalls()
    {
        for (String s: VAL_ASSIGNMENT_CALLS)
        {
            Assert.assertTrue(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code INVAL_UNARY_OPERATOR} to ensure that they all return false.
     */
    @Test
    public void testInvalUnaryOperator()
    {
        for (String s: INVAL_UNARY_OPERATOR)
        {
            Assert.assertFalse(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code INVAL_SIGNED_NUMBER} to ensure that they all return false.
     */
    @Test
    public void testInvalSignedNumber()
    {
        for (String s: INVAL_SIGNED_NUMBER)
        {
            Assert.assertFalse(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code INVAL_OBJ_FUNCTION_CALLS} to ensure that they all return false.
     */
    @Test
    public void testInvalObjFunctionCalls()
    {
        for (String s: INVAL_OBJ_FUNCTION_CALLS)
        {
            Assert.assertFalse(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code INVAL_ADVANCED_OPERATORS} to ensure that they all return false.
     */
    @Test
    public void testInvalAdvancedOperators()
    {
        for (String s: INVAL_ADVANCED_OPERATORS)
        {
            Assert.assertFalse(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code INVAL_PARENTHESIS} to ensure that they all return false.
     */
    @Test
    public void testInvalParenthesis()
    {
        for (String s: INVAL_PARENTHESIS)
        {
            Assert.assertFalse(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code INVAL_FORWARD_LOOPS} to ensure that they all return false.
     */
    @Test
    public void testInvalForwardLoop()
    {
        for (String s: INVAL_FORWARD_LOOPS)
        {
            Assert.assertFalse(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code INVAL_WHILE_LOOPS} to ensure that they all return false.
     */
    @Test
    public void testInvalWhileLoop()
    {
        for (String s: INVAL_WHILE_LOOPS)
        {
            Assert.assertFalse(Parser.parse(Tokenizer.tokenize(s)));
        }
    }

    /**
     * Tests the strings of {@code INVAL_ASSIGNMENT_CALLS} to ensure that they all return false.
     */
    @Test
    public void testInvalAssignmentCalls()
    {
        for (String s: INVAL_ASSIGNMENT_CALLS)
        {
            Assert.assertFalse(Parser.parse(Tokenizer.tokenize(s)));
        }
    }
}