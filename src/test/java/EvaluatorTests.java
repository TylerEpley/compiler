import CompilerComponents.Evaluator;
import org.junit.Assert;
import org.junit.Test;

public class EvaluatorTests
{
    String[] exprs =
            {
                    "234",
                    "1 + 3",
                    "(1 + 3) * 45",
                    "4 + 6 + (1 * 3 + 10)",
                    "1 + (2 + 1) * (78+3*15)",
                    "(1 + (2 + 1)) * 45",
                    "(1 + (2 + 1)) * (78+3*15) +45",
                    "( 1 +",
                    "1 + * 2",
                    "(1 + (5 - 2) ) * 45",
                    "( 1 + 6/2) *45",
                    "( 1 + 7%2)*45",
                    "(1 + 6.2/2)*45",
                    "(1 + .2) * 10",
                    "( (1 + 8 )// 3) * 45",
            };

//        System.out.println(exp   + " returns " + evalExpr("234")); //234
//        System.out.println(exp1  + " returns " + evalExpr("1 + 3")); //4
//        System.out.println(exp2  + " returns " + evalExpr("(1 + 3) * 45")); //180
//        System.out.println(exp3  + " returns " + evalExpr("4 + 6 + (1 * 3 + 10)")); //23
//        System.out.println(exp4  + " returns " + evalExpr("(1 + (2 + 1)) * 45")); //180
//        System.out.println(exp5  + " returns " + evalExpr("(1 + (2 + 1)) * (78+3*15) +45")); //537
//        System.out.println(exp6  + " returns " + evalExpr("( 1 +")); //null
//        System.out.println(exp7  + " returns " + evalExpr("1 + * 2")); //null
//        System.out.println(":BONUS EXPRESSIONS:");
//        System.out.println(exp8  + " returns " + evalExpr("(1 + (5 - 2) ) * 45")); //180
//        System.out.println(exp9  + " returns " + evalExpr("( 1 + 6./2) * 45")); //180.0 //Needed to add decimal
//        System.out.println(exp10 + " returns " + evalExpr("( 1 + 7%2) * 45")); //90
//        System.out.println(exp11 + " returns " + evalExpr("(1 + 6.2/2)*45")); //184.49999999999997
//        System.out.println(exp12 + " returns " + evalExpr("(1 + .2) * 10")); //12.0
//        System.out.println(exp13 + " returns " + evalExpr("( (1 + 8 )/ 3) * 45")); //135

    @Test
    public void testExprs()
    {
    }
}
