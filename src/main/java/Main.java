import FX.InterpreterController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main class that will launch the javaFx {@link Application} {@link InterpreterController} to allow the user to
 * interact with the compiler.
 */
public class Main extends Application
{
    /**
     * Method that will launch the javafx thread for the Interpreter.
     * @param args Arguments to pass into {@link InterpreterController}.
     */
    public static void main(String... args)
    {
        Application.launch(args);
    }

    /**
     * This starts up the interpreter Javafx window.
     * @param stage The primary {@link Stage} that this program will use
     * @throws Exception any and all thrown exceptions will be thrown up past here, and into our main program.
     */
    @Override
    public void start(Stage stage) throws Exception
    {
        //Load in the fxml file
        FXMLLoader fxmlLoader = new FXMLLoader( getClass().getResource("/Interpreter.fxml") );
        Parent root = fxmlLoader.load();

        //Setup The scene
        Scene scene = new Scene(root, 800, 800);
        stage.setTitle("PYTHON IDLE 0.000001");
        stage.setScene(scene);
        stage.show();
    }
}
