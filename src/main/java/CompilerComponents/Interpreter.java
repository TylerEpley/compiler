package CompilerComponents;

import Exceptions.InvalidStringException;

import java.util.Arrays;

public class Interpreter
{
    //Variables
    private static final String EMPTY_STRING_MSG = "INVALID: String must contain at least one element.";
    private static final String FAILED_PARSE_MSG = "INVALID: Unable to parse the tokenized string ";

    public static String interpret(String s) throws InvalidStringException
    {
        //If empty then throw exception.
        if (s.length() == 0)
            throw new InvalidStringException(EMPTY_STRING_MSG);
        //If invalid throw invalid string exception
        if (!Parser.parse(Tokenizer.tokenize(s)))
            throw new InvalidStringException(FAILED_PARSE_MSG + Arrays.toString(Tokenizer.tokenize(s)));
        //Were all good pony boy. Go ahead and evaluate that string.
        return Evaluator.evalExpr(s).toString();
    }
}
