package CompilerComponents;

/**
 * Static function that will break the past down string into its "Token" parts..
 */
public class Tokenizer
{
    //Determines where exactly to break the string
    private static final String REGEX_BREAKER =
            "(?<!e(\\+|-))" +             //ENSURE sure that there isn't an 'e' followed by ether a '+' or '-'
            "(?<=[-+*/=<>()%:])" +         //WHILE looking forward until a special break character is found.
            "(?! )" +                       //THEN ensure that there is no space following the break character.
            "|((?<!(e)| )" +                //OR ensure that there is no 'e' or space
            "(?=[-+*/=<>%:]))" +           //WHILE looking backwards until a special break character is found
            "|((?<! )" +                    //OR ensure that there is no space
            "(?=[()]))" +                   //WHILE looking forward for a '(' or ')'
            "|\\s+";                        //BREAK whenever there is white space.
    /**
     * Function that will break the passed down string into an array of tokens.
     * EX: (123 + 5) = [ (, 123, +, 5, ) ]
     *
     * @param s The String the user wishes to break down into tokens.
     * @return And array filled with tokens based on the passed in string.
     */
    public static String[] tokenize(String s)
    {
        //Break upon white spaces first
        return s.trim().split(REGEX_BREAKER);
    }
}
