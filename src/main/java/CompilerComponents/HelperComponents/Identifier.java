package CompilerComponents.HelperComponents;

import Enums.ParserEnum;

public class Identifier
{
    //Variables and operators
    private static final String COLON_REGEX ="^:$";
    private static final String DIVISOR_REGEX = "^/$";
    private static final String EQUALS_REGEX = "^=$";
    private static final String FUNCTION_REGEX = "^(([a-z]|[A-Z])+(\\d)*)*\\.(([a-z]|[A-Z])+(\\d)*)*$";
    private static final String LEFT_CARROT_REGEX = "^<$";
    private static final String LEFT_PARTH_REGEX = "^\\($";
    private static final String MODULUS_REGEX = "^%$";
    private static final String MULTIPLIER_REGEX = "^\\*$";
    private static final String NUMBER_REGEX = "^(\\d+.\\d+e(-|\\+)?\\d+)|(\\d*\\.?\\d*)$";
    private static final String RIGHT_CARROT_REGEX = "^>$";
    private static final String RIGHT_PARTH_REGEX = "^\\)$";
    private static final String UNARY_OPERATOR_REGEX = "^(\\+|-)$";
    private static final String VARIABLE_REGEX = "^(([a-z]|[A-Z])+(\\d)*)*$";

    //Loop arguments
    private static final String FOR_REGEX ="^for$";
    private static final String IN_REGEX ="^in$";
    private static final String RANGE_REGEX ="^range$";
    private static final String WHILE_REGEX ="^while$";

    /**
     * Function that returns the {@link ParserEnum} that the passed in String matches.
     * @param unknown The token being tested.
     * @return {@link ParserEnum} that this token matches.
     */
    public static ParserEnum whatIs(String unknown)
    {
        if (unknown.matches(WHILE_REGEX))
            return ParserEnum.whileCall;
        else if (unknown.matches(FOR_REGEX))
            return ParserEnum.forCall;
        else if (unknown.matches(IN_REGEX))
            return ParserEnum.inCall;
        else if (unknown.matches(RANGE_REGEX))
            return ParserEnum.rangeCall;
        else if(unknown.matches(NUMBER_REGEX))
            return ParserEnum.number;
        else if (unknown.matches(VARIABLE_REGEX))
            return ParserEnum.variable;

        else if(unknown.matches(FUNCTION_REGEX))
            return ParserEnum.objectFunctionCall;

        else if (unknown.matches(LEFT_PARTH_REGEX))
            return ParserEnum.leftParentheses;

        else if (unknown.matches(RIGHT_PARTH_REGEX))
            return ParserEnum.rightParenthesis;

        else if (unknown.matches(UNARY_OPERATOR_REGEX))
            return ParserEnum.unaryOperator;

        else if (unknown.matches(MULTIPLIER_REGEX))
            return ParserEnum.multiplier;

        else if (unknown.matches(DIVISOR_REGEX))
            return ParserEnum.divisor;

        else if (unknown.matches(MODULUS_REGEX))
            return ParserEnum.modulus;

        else if (unknown.matches(EQUALS_REGEX))
            return ParserEnum.equals;

        else if (unknown.matches(LEFT_CARROT_REGEX))
            return ParserEnum.leftCarrot;

        else if (unknown.matches(RIGHT_CARROT_REGEX))
            return ParserEnum.rightCarrot;

        else if (unknown.matches(COLON_REGEX))
            return ParserEnum.colon;

        //All else fails return unknown
        return ParserEnum.inValid;
    }
}
