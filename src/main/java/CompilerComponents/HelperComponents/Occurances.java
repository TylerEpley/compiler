package CompilerComponents.HelperComponents;

import Enums.ParserEnum;
import Exceptions.NotPairedException;

/**
 * Utility class used to find for the calculation of different properties of {@link Enums.ParserEnum}s. This includes
 * finding the next occurrence of a {@link Enums.ParserEnum}s. Or how many times they occurred and so on and so forth.
 */
public class Occurances
{
    /**
     * Function that will find the next number/variable, or '(' in the passed in String array.
     * @param start Where to start searching
     * @param end Where to finish search
     * @param s Tokenized String array.
     *
     * @return Where the next element in the tokenized array is. Returns -1 if it could not find one.
     */
    public static int findNextElementPos(int start, int end, String[] s)
    {
        for (int i = start +1; i <= end; i++)
        {
            if (Identifier.whatIs(s[i]) == ParserEnum.leftParentheses ||
                    Identifier.whatIs(s[i]) == ParserEnum.number ||
                    Identifier.whatIs(s[i]) == ParserEnum.objectFunctionCall ||
                    Identifier.whatIs(s[i]) == ParserEnum.variable)
                return i;
        }
        return -1;
    }

    /**
     * Function that will find the matching ')' from the start point given.
     * @param start Where to start searching
     * @param s Tokenized String array.
     *
     * @return Where the matching parenthesis in the tokenized array is. Returns -1 if it could not find one.
     */
    public static int matchingParent(int start, String[] s) throws NotPairedException {
        int leftParentCount = 0, rightParentCount = 0;
        for (int i = start; i < s.length; i++)
        {
            if (Identifier.whatIs(s[i]) == ParserEnum.leftParentheses)
                leftParentCount++;
            if (Identifier.whatIs(s[i]) == ParserEnum.rightParenthesis)
                rightParentCount++;
            //If these values equal each other then we have found the matching parenthesis index
            if (leftParentCount == rightParentCount && leftParentCount > 0)
                return i;
        }
        throw new NotPairedException("Missing right parenthesis");
    }
}
