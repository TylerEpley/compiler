package CompilerComponents;

import CompilerComponents.HelperComponents.Identifier;
import CompilerComponents.HelperComponents.Occurances;
import Enums.ParserEnum;
import Exceptions.NotPairedException;

import static Enums.ParserEnum.*;

/**
 * Class that determines whether or not a String array of tokens is valid or not.
 */
public class Parser
{
    public static void main(String[] args)
    {
        String test = "while ( 7):";
        ParserEnum[] startingArgs = {whileCall, leftParentheses};
        ParserEnum[] endArgs = {rightParenthesis, colon};
        try {
            System.out.println("Value is " +
                    matchesPattern(startingArgs, 2, test.length()-2, endArgs, Tokenizer.tokenize(test)));
        }
        catch (Exception ex)
        {
            System.out.println(ex);
        }
    }
    /**
     * Takes in a series of strings and determines whether or not they form a valid syntactical statement. Syntactical
     * may not be a word.
     * @param s  Varargs of Strings.
     * @return Boolean value that determines whether or not the entered in series of tokens are valid.
     */
    public static boolean parse(String... s)
    {
        try {
            return isExpr(0,s.length-1, s);
        } catch (NotPairedException e) {
            return false;
        }
    }

    private static boolean isExpr(int start, int end, String[] s) throws NotPairedException
    {
        //If only a single element then skip straight to <ID> rule
        if (start == end)
            return isId(start, s);
        //Valid range check
        if (end< start)
            return false;
        //Function call cases
        if (functionCallCases(start, end, s))
            return true;
        //Loop call cases
        if(loopCases(start, end, s))
            return true;
        //Assignment clauses
        if(assignmentCases(start, end, s))
            return true;

        //Variables
        int newStart = -1, newEnd = -1;

        //If there is a left parenthesis, that means it must have a matching pair, that also means that operations may be
        //performed on this pair.
        if (Identifier.whatIs(s[start]) == leftParentheses)
        {
            newStart = Occurances.matchingParent(start, s); //Determines the starting place for operator examination.
            newEnd = Occurances.findNextElementPos(newStart, end, s); //Finds next, element, or bracket.
        }
        //IF the current value can be numerical, then there maybe another value in which operators have to be applied to it.
        else if (Identifier.whatIs(s[start]) == number ||
                Identifier.whatIs(s[start]) == variable)
        {
            newEnd = Occurances.findNextElementPos(start, end, s); //Finds next, element, or bracket.
        }
        //If the current value is a unary operator, then we may be dealing with a signed number
        else if(Identifier.whatIs(s[start]) == unaryOperator)
        {
            int matchingElement = Occurances.findNextElementPos(start, end, s);
            //If it is a standard pattern, then see if the rest of the expression matches
            if (isStandardPattern(start -1, matchingElement, s))
                return isExpr(matchingElement, end, s);
        }

        //A new may be required if a parenthesis was found, this logic just ensures that a newstart is initialized correctly.
        if (newStart == -1)
            newStart = start;
        if (isStandardPattern(newStart, newEnd, s) && newEnd != -1)
            return isExpr(start, newStart, s) && isExpr(newEnd, end, s);

        //If there is not a new element then break down this current expression
        return isExpr(start +1, end -1, s);
    }

    private static boolean isStandardPattern(int start, int end, String[] s)
    {
        //unary Operator rule
        if (standardHelper(unaryOperator, Integer.MAX_VALUE, start, end, s))
            return true;
        //multiplier rule
        if (standardHelper(multiplier, 2, start, end, s))
            return true;
        //divisor rule
        if (standardHelper(divisor, 2, start, end, s))
            return true;
        //modulo rule
        if (standardHelper(modulus, 1, start, end, s))
            return true;
        return false;
    }

    private static boolean standardHelper(ParserEnum type, int max, int start, int end, String[] s)
    {
        //Position that is an actual index
        int realStart = start +1, count = 0;

        for (int i = realStart; i< end; i++)
        {
            //Count the identifier found
            if (Identifier.whatIs(s[i]) == type)
                count++;
                //If not found then fail out
            else if (Identifier.whatIs(s[i]) != type)
                return false;
            //If the count goes over the maximum allotted then fail out.
            if (count > max)
                return false;
            //If we haven't failed, and we have found a type, and we are almost done, then the pattern is standard.
            if (Identifier.whatIs(s[i]) == type && i == end -1)
                return true;
        }
        return false;
    }

    private static boolean assignmentCases(int start, int end, String[] s) throws  NotPairedException
    {
        //Special cases: ASSIGNMENTS
        if (start +2 <= end)
        {
            if (Identifier.whatIs(s[start]) == variable &&
                Identifier.whatIs(s[start + 1]) == equals)
                return isExpr(start+2, end, s);
        }
        return false;
    }

    private static boolean loopCases(int start, int end, String[] s) throws NotPairedException
    {
        //Special cases: FORWARD LOOP for variable in range(<expr>)
        if (start + 6 <= end)
        {
            //Forward loop clause
            if (Identifier.whatIs(s[start]) == forCall &&
                    Identifier.whatIs(s[start + 1]) == variable &&
                    Identifier.whatIs(s[start+2]) == inCall &&
                    Identifier.whatIs(s[start+3]) == rangeCall &&
                    Identifier.whatIs(s[start+4]) == leftParentheses &&
                    Identifier.whatIs(s[end -1]) == rightParenthesis &&
                    Identifier.whatIs(s[end]) == colon &&
                    start == 0)
            {
                return isExpr(start+4, end -1,s);
            }
        }
        //Special cases: WHILE LOOP
        if (start + 4 <= end)
        {
            //while loop clause
            if (Identifier.whatIs(s[start]) == whileCall &&
                    Identifier.whatIs(s[start + 1]) == leftParentheses &&
                    Identifier.whatIs(s[end - 1]) == rightParenthesis &&
                    Identifier.whatIs(s[end]) == colon &&
                    start == 0)
            {
                return isExpr(start+2, end-2,s);
            }
        }
        return false;
    }
    private static boolean functionCallCases(int start, int end, String[] s) throws NotPairedException {
        //Special cases: FUNCTION CALLs
        if (Identifier.whatIs(s[start]) == objectFunctionCall ||
                Identifier.whatIs(s[start]) == variable)
        {
            //Special rule: empty call.
            if (start +2 <= end &&
                    Identifier.whatIs(s[start+1]) == leftParentheses &&
                    Identifier.whatIs(s[start+2]) == rightParenthesis)
            {
                int nextElementPos = Occurances.findNextElementPos(start+2, end, s);
                //If there are no other arguments then just return true.
                if (nextElementPos == -1 && start+2 == end)
                    return true;
                //Otherwise if the remaining arguments fall into standard pattern, then see if they are an expression.
                if (isStandardPattern(start+2, nextElementPos, s))
                    return isExpr(nextElementPos, end, s);
            }
            //Special rule: function call with arguments afterwards.
            else if (start +2 < end &&
                    Identifier.whatIs(s[start+1]) == leftParentheses)
                return isExpr(start+1, end, s);
        }
        return false;
    }

    private static boolean matchesPattern(ParserEnum[] startArgs, int exprStart, int exprEnd, ParserEnum[] endArgs, String[] tokenString) throws NotPairedException {
        //See if start of the sequences matches that pattern given.
        for(int i = 0; i < exprStart; i++)
        {
            //safe range catch
            if (i >= tokenString.length )
                return false;
            //Otherwise check
            if (Identifier.whatIs(tokenString[i]) != startArgs[i])
                return false;
        }

        //See if the end of the sequence matches
        for (int i = tokenString.length; i > exprEnd -2; i--)
        {
            //Safe range check
            if (i < tokenString.length)
                return false;
            //Otherwise check
            //Otherwise check
            if (Identifier.whatIs(tokenString[i]) != startArgs[i])
                return false;
        }

        //check if expr is correct
        return isExpr(exprStart, exprEnd, tokenString);
    }

    private static boolean isId(int start, String[] s)
    {
        return Identifier.whatIs(s[start]) == number ||
                Identifier.whatIs(s[start]) == variable;
    }
}