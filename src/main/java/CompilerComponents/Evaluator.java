package CompilerComponents;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Evaluator {

    public static Object evalExpr(String s) {

        try {

            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine engine = manager.getEngineByName("js");
            return engine.eval(s);}

        catch (ScriptException e)
        {
            return "INVALID: Oops, haven't programed the logic for this yet...";}
    }
}
