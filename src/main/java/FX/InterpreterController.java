package FX;

import CompilerComponents.Interpreter;
import Exceptions.InvalidStringException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

public class InterpreterController implements Initializable
{
    //FMXL elements
    @FXML
    AnchorPane mainPane;
    @FXML
    TextArea inputArea;
    @FXML
    TextArea outputArea;
    @FXML
    TextField colLineField;
    @FXML
    MenuBar topBar;
    @FXML
    Menu optionMenu;
    @FXML
    MenuItem closeMenuItem;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        setUpListeners();
    }

    /**
     * Sets up any listeners related to the fxml that this class controls.
     */
    private void setUpListeners()
    {
        inputArea.setOnKeyPressed(e ->
        {
            //If input the input keystroke was ENTER then process commands.
            if (e.getCode() == KeyCode.ENTER)
            {
                processInput();
            }
        });
    }

    /**
     * Takes the String found in {@code inputArea} and attempts to process it in the {@link Interpreter}. Produces either
     * the evaluation for String, or the error message that the string produces.
     */
    private void processInput()
    {
        String answer;
        try
        {
            answer = Interpreter.interpret(inputArea.getText());
        } catch (InvalidStringException e)
        {
            answer = e.getMessage();
        }

        //output
        outputArea.setText(answer);
    }
}
