package Enums;

/**
 * Enums used to represent the various states a token can be.
 */
public enum ParserEnum
{
    inValid,
    number,
    variable,
    leftParentheses,
    rightParenthesis,
    unaryOperator,
    multiplier,
    divisor,
    modulus,
    equals,
    leftCarrot,
    rightCarrot,
    objectFunctionCall,
    whileCall,
    forCall,
    inCall,
    rangeCall,
    colon,
}
