package Exceptions;

/**
 * Exception called whenever a left Parenthesis does not have it's matching pair.
 */
public class NotPairedException extends Exception
{
    public NotPairedException(String message)
    {
        super(message);
    }
}
