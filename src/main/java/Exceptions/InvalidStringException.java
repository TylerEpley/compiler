package Exceptions;

/**
 * {@link Exception} to be thrown if a given string is unable to be interpreted by the {@link CompilerComponents.Interpreter}
 */
public class InvalidStringException extends Exception
{
    public InvalidStringException(String message)
    {
        super(message);
    }
}
